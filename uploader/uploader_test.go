/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package uploader

import (
	"errors"
	"reflect"
	"strings"
	"sync"
	"testing"
)

func Test_upload_all_files(t *testing.T) {
	// given
	hostUrl := "http://hello"
	filesPath := []string{"a", "b", "c", "d", "e", "f", "g"}
	var uploaded []string
	var hostUsed string
	var mutex = &sync.Mutex{}

	uploadClient := func(filePath string, fileNamedClasses []string, host string) error {
		mutex.Lock()
		hostUsed = host
		uploaded = append(uploaded, filePath)
		mutex.Unlock()
		return nil
	}

	// when
	jobErrors := upload(filesPath, hostUrl, uploadClient, nil)

	// then
	if len(jobErrors) != 0 {
		t.Errorf("not expected errors")
	}
	if hostUsed != hostUrl {
		t.Errorf("not expected host: " + hostUsed)
	}

	if len(uploaded) != len(filesPath) {
		t.Errorf("not expected uploaded files: " + strings.Join(uploaded, " : "))
	}

	for _, filePath := range filesPath {
		found := false
		for _, uploadedFilePath := range uploaded {
			if filePath == uploadedFilePath {
				found = true
			}
		}
		if !found {
			t.Errorf("not expected uploaded files: " + strings.Join(uploaded, " : "))
		}
	}
}

func Test_upload_all_files_with_files_named_classes(t *testing.T) {
	// given
	hostUrl := "http://hello"
	filesPath := []string{"a", "b", "c", "d", "e", "f", "g"}
	filesNamedClasses := make(map[string][]string)
	for _, filePath := range filesPath {
		filesNamedClasses[filePath] = []string{"named class of " + filePath, "named class 2 of " + filePath}
	}
	var uploadedFilesPath []string
	var uploadedFilesNamedClasses = make(map[string][]string)
	var hostUsed string
	var mutex = &sync.Mutex{}

	uploadClient := func(filePath string, fileNamedClasses []string, host string) error {
		mutex.Lock()
		hostUsed = host
		uploadedFilesPath = append(uploadedFilesPath, filePath)
		uploadedFilesNamedClasses[filePath] = fileNamedClasses
		mutex.Unlock()
		return nil
	}

	// when
	jobErrors := upload(filesPath, hostUrl, uploadClient, filesNamedClasses)

	// then
	if len(jobErrors) != 0 {
		t.Errorf("not expected errors")
	}
	if hostUsed != hostUrl {
		t.Errorf("not expected host: " + hostUsed)
	}

	if len(uploadedFilesPath) != len(filesPath) {
		t.Errorf("not expected uploaded files: " + strings.Join(uploadedFilesPath, " : "))
	}

	for _, filePath := range filesPath {
		found := false
		for _, uploadedFilePath := range uploadedFilesPath {
			if filePath == uploadedFilePath {
				found = true
			}
		}
		if !found {
			t.Errorf("not expected uploaded files: " + strings.Join(uploadedFilesPath, " : "))
		}
		if !reflect.DeepEqual(uploadedFilesNamedClasses[filePath], []string{"named class of " + filePath, "named class 2 of " + filePath}) {
			t.Errorf("not expected file named class : '" + strings.Join(uploadedFilesNamedClasses[filePath], ", ") + "' for file : " + filePath)
		}
	}
}

func Test_try_to_upload_all_files_even_if_errors_occur(t *testing.T) {
	// given
	hostUrl := "http://hello"
	filesPath := []string{"a", "b"}
	var uploaded []string

	uploadClient := func(filePath string, fileNamedClasses []string, host string) error {
		return errors.New("an error")
	}

	// when
	jobErrors := upload(filesPath, hostUrl, uploadClient, nil)

	// then
	if len(uploaded) != 0 {
		t.Errorf("not expected uploaded files: " + strings.Join(uploaded, " : "))
	}

	var filePathsError []string
	for _, jobError := range jobErrors {
		filePathsError = append(filePathsError, jobError.filePath)
	}
	if len(filePathsError) != len(filesPath) {
		t.Errorf("not expected job errors: " + strings.Join(filePathsError, " : "))
	}
}

func Test_retry_several_times_if_error_occurs(t *testing.T) {
	// given
	hostUrl := "http://hello"
	filesPath := []string{"a"}
	var uploaded []string
	var mutex = &sync.Mutex{}
	var errorCount = 0

	uploadClient := func(filePath string, fileNamedClasses []string, host string) error {
		mutex.Lock()
		errorCount++
		if errorCount == maxRetries {
			// eventually we manage to upload the file
			uploaded = append(uploaded, filePath)
			return nil
		}
		mutex.Unlock()
		return errors.New("raise an error")
	}

	// when
	var jobErrors = upload(filesPath, hostUrl, uploadClient, nil)

	// then
	if len(uploaded) != 1 {
		t.Errorf("file was not uploaded")
	}
	if len(jobErrors) != 0 {
		t.Errorf("no errors should be raised since file should have been uploaded eventually")
	}
}

/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package uploader

import (
	"bytes"
	"errors"
	"io"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

const endpoint = "/api/upload"
const apiKey = "Basic Q2hyaXN0b3BoZTpNYWxkaXZp"

func CheckRemote(hostUrl string) error {
	_, err := http.Head(hostUrl)
	return err
}

func uploadFile(filePath string, fileNamedClasses []string, hostUrl string) error {
	file, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer file.Close()

	body := &bytes.Buffer{}
	writer := multipart.NewWriter(body)
	part, err := writer.CreateFormFile("file", filepath.Base(file.Name()))
	if err != nil {
		return err
	}

	_, err = io.Copy(part, file)
	if err != nil {
		return err
	}

	err = writer.Close()
	if err != nil {
		return err
	}

	r, err := http.NewRequest("POST", hostUrl+endpoint, body)
	if err != nil {
		return err
	}

	// TODO test
	if len(fileNamedClasses) != 0 {
		joinedNamedClasses := strings.Join(fileNamedClasses[:], ",")
		q := r.URL.Query()
		q.Add("file-named-class", joinedNamedClasses)
		r.URL.RawQuery = q.Encode()
	}
	//

	r.Header.Add("Content-Type", writer.FormDataContentType())
	r.Header.Add("Authorization", apiKey)
	client := &http.Client{}
	var response *http.Response
	response, err = client.Do(r)
	if err != nil {
		return err
	}
	if response.StatusCode != 200 {
		return errors.New("ERROR, uploading file '" + filePath + "', error: " + response.Status)
	}

	return nil
}

/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package uploader

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func Test_raise_error_if_remote_is_not_reachable(t *testing.T) {
	// given
	filePath := "../test_dir/subdir/b.png"

	// when
	var err error
	err = uploadFile(filePath, nil, "https://abc")

	// then
	if err == nil {
		t.Errorf("error was expected")
		return
	}
	if !strings.Contains(err.Error(), "Post \"https://abc/api/upload\": dial tcp: lookup abc") {
		t.Errorf("not expected error: " + err.Error())
		return
	}
}

// TODO: add test with file named classes

func Test_upload_file(t *testing.T) {
	// given
	var actualReq http.Request
	var actualReqBody string
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		actualReq = *req
		bodyBytes, _ := ioutil.ReadAll(req.Body)
		actualReqBody = string(bodyBytes[:])
		rw.Write([]byte(`OK`))
	}))
	defer server.Close()

	filePath := "../test_dir/hélû.txt"

	// when
	var err error
	err = uploadFile(filePath, nil, server.URL)

	// then
	if err != nil {
		t.Errorf("error was not expected: " + err.Error())
		return
	}
	actualEndPoint := actualReq.URL.String()
	actualApiKey := actualReq.Header.Get("Authorization")
	actualContentType := actualReq.Header.Get("Content-Type")
	if actualEndPoint != "/api/upload" {
		t.Errorf("url not expected: " + actualEndPoint)
		return
	}
	if actualApiKey != "Basic Q2hyaXN0b3BoZTpNYWxkaXZp" {
		t.Errorf("apikey not expected: " + actualApiKey)
		return
	}
	if !strings.Contains(actualContentType, "multipart/form-data; boundary=") {
		t.Errorf("actual ContentType not expected: " + actualContentType)
		return
	}
	if actualReq.Method != "POST" {
		t.Errorf("actual http method not expected: " + actualReq.Method)
		return
	}
	if !strings.Contains(actualReqBody, "Content-Disposition: form-data; name=\"file\"; filename=\"hélû.txt\"") {
		t.Errorf("request body NOK: " + actualReqBody)
		return
	}
	if !strings.Contains(actualReqBody, "Content-Type: application/octet-stream") {
		t.Errorf("request body NOK: " + actualReqBody)
		return
	}
	if !strings.Contains(actualReqBody, "content éçùè period!") {
		t.Errorf("request body NOK: " + actualReqBody)
		return
	}
}

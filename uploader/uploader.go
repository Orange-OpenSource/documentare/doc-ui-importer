// Package uploader /*
package uploader

import (
	"fmt"
	"strconv"
	"strings"
	"sync"
	"time"
)

const noOfWorkers = 64
const maxRetries = 5

type uploadJob struct {
	filePath          string
	filesNamedClasses []string
}
type jobError struct {
	filePath string
}

func worker(wg *sync.WaitGroup, uploads chan uploadJob, results chan jobError, host string, uploadClient func(filePath string, fileNamedClasses []string, host string) error) {
	for job := range uploads {
		err := doUpload(job.filePath, job.filesNamedClasses, uploadClient, host)
		if err != nil {
			results <- jobError{job.filePath}
		}
	}
	wg.Done()
}

func launchUploadWorkers(uploads chan uploadJob, results chan jobError, host string, uploadClient func(filePath string, fileNamedClasses []string, host string) error) {
	var wg sync.WaitGroup
	for i := 0; i < noOfWorkers; i++ {
		wg.Add(1)
		go worker(&wg, uploads, results, host, uploadClient)
	}
	wg.Wait()
	close(results)
}

func initUploads(filesPath []string, filesNamedClasses map[string][]string) chan uploadJob {
	uploads := make(chan uploadJob, len(filesPath))
	for _, filePath := range filesPath {
		job := uploadJob{filePath, filesNamedClasses[filePath]}
		uploads <- job
	}
	close(uploads)
	return uploads
}

// TODO: test filesNamedClasses
func Upload(filesPath []string, host string, filesNamedClasses map[string][]string) {
	upload(filesPath, host, uploadFile, filesNamedClasses)
}

func doUpload(filePath string, fileNamedClasses []string, uploadClient func(filePath string, fileNamedClasses []string, host string) error, host string) error {
	errorCount := 0
	var err error
	for errorCount <= maxRetries {
		err = uploadClient(filePath, fileNamedClasses, host)
		if err != nil {
			errorCount++
			fmt.Println("⚠️ WARN failed to upload '" + filePath + "' (" + err.Error() + ")")
			fmt.Println("🔵 we will retry to upload it")
		} else {
			fmt.Println("🟢 " + filePath + " - " + strings.Join(fileNamedClasses, ", "))
			return nil
		}
	}
	if err != nil {
		fmt.Println("🛑 ERROR failed to upload '" + filePath + "'; we will not retry anymore")
	}
	return err
}

func upload(filesPath []string, host string, uploadClient func(filePath string, fileNamedClasses []string, host string) error, filesNamedClasses map[string][]string) []jobError {
	uploads := initUploads(filesPath, filesNamedClasses)
	jobErrorsChan := make(chan jobError)
	doneChan := make(chan bool)

	var jobErrors []jobError
	go func() {
		for jobError := range jobErrorsChan {
			jobErrors = append(jobErrors, jobError)
		}
		doneChan <- true
	}()

	start := time.Now()
	launchUploadWorkers(uploads, jobErrorsChan, host, uploadClient)
	duration := time.Since(start)

	<-doneChan

	fmt.Println("\nTime to upload files: " + duration.Truncate(time.Millisecond).String())
	fmt.Println("Uploaded " + strconv.Itoa(len(filesPath)-len(jobErrors)) + " files\n")

	for _, jobError := range jobErrors {
		fmt.Println("==> FAILED to upload file " + jobError.filePath)
	}

	return jobErrors
}

/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package main

import (
	"strings"
	"testing"
)

func Test_raise_an_error_if_csv_input_file_is_empty(t *testing.T) {
	// when
	err := checkInputParameters("", "http://remote")

	// then
	if err == nil {
		t.Errorf("error was expected since csvinput is empty")
		return
	}
	expectedError := "🔴 ERROR: csvinput parameter not provided"
	if err.Error() != expectedError {
		t.Errorf("unexpected error: '" + err.Error() + "' instead of '" + expectedError + "'")
		return
	}
}

func Test_raise_an_error_if_remote_is_empty(t *testing.T) {
	// when
	err := checkInputParameters("/tmp/csvinput", "")

	// then
	if err == nil {
		t.Errorf("error was expected since remote is empty")
		return
	}
	expectedError := "🔴 ERROR: remote parameter not provided"
	if err.Error() != expectedError {
		t.Errorf("unexpected error: '" + err.Error() + "' instead of '" + expectedError + "'")
		return
	}
}

func Test_raise_an_error_if_csv_input_file_does_not_exist(t *testing.T) {
	// when
	err := checkInputParameters("/root/toto", "http://remote")

	// then
	if err == nil {
		t.Errorf("error was expected since csvinput file does not exist")
		return
	}
	expectedError := "🔴 ERROR: can not open csv input file (/root/toto), error: 'open /root/toto: no such file or directory'"
	if err.Error() != expectedError {
		t.Errorf("unexpected error: '" + err.Error() + "' instead of '" + expectedError + "'")
		return
	}
}

func Test_raise_an_error_if_remote_endpoint_is_not_available(t *testing.T) {
	// when
	err := checkInputParameters("test_csvinput/files-paths-with-named-classes.csv", "https://l")

	// then
	if err == nil {
		t.Errorf("error was expected since remote is not valid")
		return
	}
	expectedError := "🔴 ERROR: failed to talk to remote (https://l), error: 'Head \"https://l\""
	if !strings.Contains(err.Error(), expectedError) {
		t.Errorf("unexpected error: '" + err.Error() + "' instead of '" + expectedError + "'")
		return
	}
}

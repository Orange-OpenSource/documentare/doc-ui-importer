// Package csvinput /*
package csvinput

import (
	"encoding/csv"
	"fmt"
	"os"
	"strings"
)

func LoadCSVInput(csvInputFilePath string) ([]string, map[string][]string, error) {
	f, err := os.Open(csvInputFilePath)
	if err != nil {
		fmt.Println("Unable to read input file "+csvInputFilePath, err)
		return nil, nil, err
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	// TODO: test error
	if err != nil {
		fmt.Println("Unable to parse file as CSV for "+csvInputFilePath, err)
		return nil, nil, err
	}

	filesPath := make([]string, len(records))
	filesNamedClasses := make(map[string][]string)
	for index, csvLineStrings := range records {
		filePath := strings.TrimSpace(csvLineStrings[0])
		filesPath[index] = filePath
		if len(csvLineStrings) > 1 {
			filesNamedClasses[filePath] = csvLineStrings[1:]
		}
	}

	return filesPath, filesNamedClasses, nil
}

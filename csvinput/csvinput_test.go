package csvinput

import (
	"reflect"
	"strings"
	"testing"
)

func Test_extract_files_to_import_paths_from_csv(t *testing.T) {
	// given
	csvInputFilePath := "../test_csvinput/files-paths.csv"

	// when
	var filesPath []string
	var err error
	filesPath, _, err = LoadCSVInput(csvInputFilePath)

	// then
	if err != nil {
		t.Errorf("error was not expected: " + err.Error())
		return
	}
	expectedFilesAbsPath := []string{"titi.txt", "rhooo_minet.png"}
	if !reflect.DeepEqual(filesPath, expectedFilesAbsPath) {
		t.Errorf("filesAbsPath not expected: %s", filesPath)
	}
}

func Test_extract_file_named_classes_along_file_path(t *testing.T) {
	// given
	csvInputFilePath := "../test_csvinput/files-paths-with-named-classes.csv"

	// when
	var filesPath []string
	var filesNamedClasses map[string][]string
	var err error
	filesPath, filesNamedClasses, err = LoadCSVInput(csvInputFilePath)

	// then
	if err != nil {
		t.Errorf("error was not expected: " + err.Error())
		return
	}
	expectedFilesNamedClasses := make(map[string][]string)
	expectedFilesNamedClasses["titi.txt"] = []string{"bird", "yellow"}
	expectedFilesNamedClasses["rhooo_minet.png"] = []string{"cat", "black and white"}
	if !reflect.DeepEqual(filesNamedClasses, expectedFilesNamedClasses) {
		t.Errorf("filesNamedClasses not expected: %s", filesPath)
	}
}

func Test_raise_error_if_file_does_not_exist(t *testing.T) {
	// given
	csvInputFilePath := "unknown-file"

	// when
	var err error
	_, _, err = LoadCSVInput(csvInputFilePath)

	// then
	if err == nil {
		t.Errorf("error was expected")
		return
	}
	if !strings.Contains(err.Error(), "open unknown-file: no such file or directory") {
		t.Errorf("not expected error: " + err.Error())
		return
	}
}

func Test_raise_error_if_file_is_not_a_valid_csv_file(t *testing.T) {
	// given
	csvInputFilePath := "../test_csvinput/not_a_valid_csv_file"

	// when
	var err error
	_, _, err = LoadCSVInput(csvInputFilePath)

	// then
	if err == nil {
		t.Errorf("error was expected")
		return
	}
	if !strings.Contains(err.Error(), "record on line 3: wrong number of fields") {
		t.Errorf("not expected error: " + err.Error())
		return
	}
}

#!/bin/sh

echo Build for Linux
env GOOS=linux GOARCH=amd64 go build -o doc-ui-importer-linux

echo Build for Macos Intel
env GOOS=darwin GOARCH=amd64 go build -o doc-ui-importer-mac-intel

echo Build for Macos Arm
env GOOS=darwin GOARCH=arm64 go build -o doc-ui-importer-mac-arm

echo Build for windows
env GOOS=windows GOARCH=amd64 go build -o doc-ui-importer.exe

/*
 * Copyright (c) 2020 Orange
 *
 * Authors: Denis Boisset, Christophe Maldivi & Joel Gardes
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by
 * the Free Software Foundation.
 */
package main

import (
	"doc-ui-importer/csvinput"
	"doc-ui-importer/uploader"
	"errors"
	"flag"
	"fmt"
	"os"
	"runtime/debug"
)

func getGitCommit() string {
	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			if setting.Key == "vcs.revision" {
				return setting.Value
			}
		}
	}
	return ""
}

func main() {
	gitCommit := getGitCommit()
	if gitCommit == "" {
		gitCommit = "unknown"
	} else {
		gitCommit = gitCommit[:10]
	}
	fmt.Println("Welcome to doc-ui-importer (git-sha1-abbrev-" + gitCommit + ")")

	var filesPath []string
	var filesNamedClasses map[string][]string
	var remoteHost string
	var err error

	var csvInputFilePath string
	flag.StringVar(&csvInputFilePath, "csvinput", "", "CSV input file path")
	flag.StringVar(&remoteHost, "remote", "", "Remote Host URL")
	flag.Parse()

	err = checkInputParameters(csvInputFilePath, remoteHost)
	if err != nil {
		fmt.Println(err)
		usage()
		os.Exit(1)
	}

	fmt.Println("Import from file: " + csvInputFilePath)
	fmt.Println("Remote host: " + remoteHost)

	filesPath, filesNamedClasses, err = csvinput.LoadCSVInput(csvInputFilePath)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	uploader.Upload(filesPath, remoteHost, filesNamedClasses)
	os.Exit(0)
}

func usage() {
	fmt.Print(`
Usage example: ./doc-ui-importer -csvinput myfile.csv -remote http://localhost:8080
`)
}

func checkInputParameters(csvInputFilePath string, remote string) error {
	if len(csvInputFilePath) == 0 {
		return errors.New("🔴 ERROR: csvinput parameter not provided")
	}
	if len(remote) == 0 {
		return errors.New("🔴 ERROR: remote parameter not provided")
	}

	csvInputFile, err := os.Open(csvInputFilePath)
	if err != nil {
		return errors.New("🔴 ERROR: can not open csv input file (" + csvInputFilePath + "), error: '" + err.Error() + "'")
	}
	defer func() { _ = csvInputFile.Close() }()

	err = uploader.CheckRemote(remote)
	if err != nil {
		return errors.New("🔴 ERROR: failed to talk to remote (" + remote + "), error: '" + err.Error() + "'")
	}

	return nil
}
